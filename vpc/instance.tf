resource "aws_instance" "example" {
    ami = "${lookup(var.AMIS,var.AWS_REGION)}"
    instance_type = "t2.micro"

# the VPC Subnet
subnet_id = "${aws_subnet.main-public-1.id}"

# Private IP
private_ip = "10.0.1.4"  #within the range of subnet main-public-1


# Security Groups
vpc_security_group_ids = ["${aws_security_group.allow-ssh.id}"]

# public SSH key
key_name = "${aws_key_pair.mykeypair.key_name}"
}

# user data
#user_data = <<-EOT
  # echo "USER_DATA = ${data.template_cloudinit_config.cloudinit-example.rendered}"
#EOT

resource "aws_ebs_volume" "ebs-volume-1" {
    availability_zone = "us-east-2c"
    size =  20
    type  = "gp2"
    tags = {
       Name = "extra volume data"
    }
}

resource "aws_volume_attachment" "ebs-vol-1-attachment" {
    device_name ="${var.INSTANCE_DEVICE_NAME}"
    volume_id = "${aws_ebs_volume.ebs-volume-1.id}"
    instance_id = "${aws_instance.example.id}"
}

# ELastic IP (EIP)
#resource "aws_eip" "example-eip" {
 #   instance_id = "${aws_instane.example.id}"
  #  vpc = true
#}
