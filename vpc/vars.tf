variable "AWS_ACCESS_KEY" {}
variable "AWS_SECRET_KEY" {}

variable "AWS_REGION" {
    default = "us-east-2"
}

variable "INSTANCE_DEVICE_NAME" {
    default = "/dev/xvdh"
  
}

variable "AMIS" {
    type = "map"
    default = {
        us-east-2 = "ami-cfdafaaa"
    }
}